# [package_notifier](https://github.com/bostrot/steemsearchapi)

A package release notifier for developers.

## Download

[Play Store](https://play.google.com/store/apps/details?id=com.bostrot.packagenotifier) or Releases.

## Screenshots
<table>
<tr>
  <td><img width="240" src="https://i.imgur.com/oWxuzgy.jpg"></td>
  <td><img width="240" src="https://i.imgur.com/iaCqrxy.jpg"></td>
  <td><img width="240" src="https://i.imgur.com/nsUKRjT.jpg"></td>
</tr>
<tr>
  <td><img width="240" src="https://i.imgur.com/a7TrJ8J.jpg"></td>
  <td><img width="240" src="https://i.imgur.com/gkEBZ00.jpg"></td>
  <td><img width="240" src="https://i.imgur.com/a7TrJ8J.jpg"></td>
</tr>
</table>

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).

## Help

Join the forum if you need help: [discuss.bostrot.com](https://discuss.bostrot.com)

You are welcome to contribute with pull requests, bug reports, ideas and donations.

Bitcoin: [1ECPWeTCq93F68BmgYjUgGSV11XuzSPSeM](https://www.blockchain.com/btc/payment_request?address=1ECPWeTCq93F68BmgYjUgGSV11XuzSPSeM&currency=USD&nosavecurrency=true&message=Bostrot)

PayPal: [paypal.me/bostrot](https://paypal.me/bostrot)

Hosting: [2.50$ VPS at VULTR](https://www.vultr.com/?ref=7505919)
